radian6-php-wrapper
=====================

Unofficial Radian6 PHP Wrapper (v.0.1)

This wrapper class is compatible with the [Radian6 API](http://socialcloud.radian6.com/docs/read/Home)

This repository contains the PHP SDK that allows you to
access the Radian6 platform from your PHP app.  


Usage
-----

The minimum you'll need to have is:

    require 'radian6-php-wrapper/src/radian.php';

    // Instantiate a new Radian6 object
    $this->radian = new Radian(
      array(
        'appKey' => $appKey
      )
    );

    $result = $this->radian->auth(
      "/authenticate",
      "get",
      array(
        'auth_user' => $appUser,
        'auth_pass' => $appPassword
      ),
      array(
        'fields' => 'userdetails,clientattributes'
      )
    );

    // Set the Radian6 Auth Token to be used by the app.
    $this->radian->setAuthToken($result->token);

To make API calls:

    // Return the data for a specified widget.
    try {
      $result = $this->radian->api(
        "/widget/{$this->widgets[$key]['widgetId']}",
        "get",
        array(),
        array()
      );
    } catch (Radian6ApiException $e) {
      error_log($e);
    }
