<?php
/**
 * Radian6 API Wrapper Class
 */

/**
 * Check for cURL, JSON and SimpleXML PHP dependencies. 
 * Ensure that the following "curl_init", "json_decode" and "simplexml_load_string" PHP 
 * functions are available to the application.
 */
if (!function_exists('curl_init')) {
	throw new Exception('Radian6 needs the CURL PHP extension.');
}

if (!function_exists('json_decode')) {
	throw new Exception('Radian6 needs the JSON PHP extension.');
}

if (!function_exists('simplexml_load_string')) {
	throw new Exception('Radian6 needs the SimpleXML PHP extension.');
}

/**
 * Thrown when an API call returns an exception.
 *
 * TODO: Confirm format of erroroneous responses.
 */
class Radian6ApiException extends Exception {
	/**
	 * The result from the API server that represents the exception information.
	 */
	protected $result;

	/**
	 * Make a new API Exception with the given result.
	 *
	 * @param array $result The result from the API server
	 */
	public function __construct($result) {
		$this->result = $result;

		$code = isset($result['error_code']) ? $result['error_code'] : 0;

		if (isset($result['error_description'])) {
			// OAuth 2.0 Draft 10 style
			$msg = $result['error_description'];
		} else if (isset($result['error']) && is_array($result['error'])) {
			// OAuth 2.0 Draft 00 style
			$msg = $result['error']['message'];
		} else if (isset($result['error_msg'])) {
			// Rest server style
			$msg = $result['error_msg'];
		} else {
		  	$msg = 'Unknown Error. Check getResult()';
		}

		parent::__construct($msg, $code);
	}

	/**
	 * Return the associated result object returned by the API server.
	 *
	 * @return ( mixed ) The result from the API server
	 */
	public function getResult() {
		return $this->result;
	}

	/**
	 * Returns the associated type for the error. This will default to
	 * 'Radian6APIException' when a type is not available.
	 *
	 * @return ( str )
	 */
	public function getType() {
		if (isset($this->result['error'])) {
			$error = $this->result['error'];
			
			if (is_string($error)) {
				// OAuth 2.0 Draft 10 style
				return $error;
			} else if (is_array($error)) {
				// OAuth 2.0 Draft 00 style
				if (isset($error['type'])) {
		  			return $error['type'];
				}
			}
		}

    	return 'Radian6APIException';
	}

	/**
	 * To make debugging easier.
	 *
	 * @return string The string representation of the error
	 */
	public function __toString() {
		$str = $this->getType() . ': ';
		if ($this->code != 0) {
			$str .= $this->code . ': ';
		}
		return $str . $this->message;
	}
}

/**
 * Simple Radian class used to make requests to the Radian6 API
 */
class Radian {
	/**
  	 * Version.
  	 */
  	const VERSION = '0.1';

	/**
	 * Default options for curl.
	 */
	public static $CURL_OPTS = array(
		CURLOPT_CONNECTTIMEOUT => 10,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_TIMEOUT        => 60,
		CURLOPT_USERAGENT      => 'radian6-php-0.1',
	);

	/**
   	 * The App Key.
   	 *
   	 * @var string
   	 */
  	protected $appKey;

  	/**
   	 * The user auth token.
   	 *
   	 * @var string
   	 */
  	protected $authToken = null;

  	/**
  	 *	The base API URL
  	 *
  	 *	@var string
  	 */
  	protected $baseApiUrl = "https://api.radian6.com/socialcloud/v1/data";

  	/**
  	 *	The base auth URL
  	 *
  	 *	@var string
  	 */
  	protected $baseAuthUrl = "https://api.radian6.com/socialcloud/v1/auth";

  	/**
  	 * HTTP Status Code
  	 *
  	 * @var string
  	 */
  	public $httpStatusCode = "";

  	/**
  	 * cURL Request
  	 *
  	 * @var string
  	 */
  	public $curlRequest = "";

	/**
	 * Initialise an Radian6 application
	 * 
	 * Configuration :
	 * - appKey : the application API key
	 *
	 * @param ( mixed ) $config
	 * @return void
	 */
	public function __construct($config) {
		$this->setAppKey($config['appKey']);

		$this->setProxy();
	}

	/**
	 * Set additional CURL Proxy options if required.
	 */
	protected function setProxy() {
		/**
		 * Determine if the Proxy Server should be used. Proxy Server should NOT be used 
		 * for local implementations. If the Proxy Server should be used, then merge the
		 * Proxy cURL configuration variables with the default configuration.
		 */
		if (Configure::read('useProxy') === true) {
			$proxyOpts = array(
			       CURLOPT_PROXYPORT => '8080',
			       CURLOPT_PROXYTYPE => 'HTTP',
			       CURLOPT_PROXY => 'http-proxy.amer.consind.ge.com',
			);

			self::$CURL_OPTS = self::$CURL_OPTS + $proxyOpts;
		}
	}

	/**
	 * Set the App API Key.
	 *
	 * @param ( str ) $appKey The Application API Key
	 * @return ( obj ) Radian
	 */
	public function setAppKey($appKey) {
		$this->appKey = $appKey;
		return $this;
	}

	/**
	 * Set the Auth Token.
	 *
	 * @param ( str ) $authToken The Radian6 Auth Token
	 * @return ( obj ) Radian
	 */
	public function setAuthToken($authToken) {
		$this->authToken = $authToken;
		return $this;
	}

	/**
	 * Build the API request parameters and parse the response.
	 *
	 * @param ( str ) $endPoint
	 * @param ( str ) $requestMethod
	 * @param ( mixed ) $headers
	 * @param ( mixed ) $params
	 * @return ( mixed ) $result
	 */
	public function api($endPoint, $requestMethod = 'get', $headers = array(), $params = array()) {
		// Build the request URL from the baseApiUrl and the endpoint passed to the function
		$url = $this->baseApiUrl . $endPoint;
		
		/**
		 * Merge the Auth Token and App Key into the request headers.
		 */ 
		$headers = !is_null($this->authToken) ? array_merge(array('auth_token' => $this->authToken), $headers) : $headers;
		$headers = !is_null($this->appKey) ? array_merge(array('auth_appkey' => $this->appKey), $headers) : $headers;
		
		/**
		 * Make the request and parse the XML response. 
		 */
		// $result = json_decode($this->makeRequest($url, $requestMethod, $params), true);
		$result = $this->makeRequest($url, $requestMethod, $headers, $params);
		
		/**
		  * Parse the XML response as a SimpleXML Obect.
		  * Ensure that CDATA is considered.
		  */ 
		$result = simplexml_load_string($result, null, LIBXML_NOCDATA);
		$result = $this->xmlToArray($result);

		// Throw an exception for any Radian6 API errors
		if ($this->httpStatusCode !== 200) {
			$e = new Radian6ApiException(
				array(
					'error_code' => $this->httpStatusCode,
					'error' => array(
						'message' => $result[0],
						'type' => 'Radian6ApiException',
					),
				)
			);

			throw $e;
		}

		return $result;
	}

	/**
	 * Build the API Auth request parameters and parse the response.
	 *
	 * @param ( str ) $endPoint
	 * @param ( str ) $requestMethod
	 * @param ( mixed ) $headers
	 * @param ( mixed ) $params
	 * @return ( mixed ) $result
	 */
	public function auth($endPoint, $requestMethod = 'get', $headers = array(), $params = array()) {
		// Build the request URL from the baseApiUrl and the endpoint passed to the function
		$url = $this->baseAuthUrl . $endPoint;
		
		/**
		 * Merge the App Key into the request headers.
		 */ 
		$headers = !is_null($this->appKey) ? array_merge(array('auth_appkey' => $this->appKey), $headers) : $headers;
		
		/**
		 * Make the request and parse the XML response. 
		 */
		$result = $this->makeRequest($url, $requestMethod, $headers, $params);
		
		/**
	  	 * Parse the XML response as a SimpleXML Obect.
		 * Ensure that CDATA is considered.
		 */ 
		$result = simplexml_load_string($result, null, LIBXML_NOCDATA);

		// Throw an exception for any Radian6 API errors
		if ($this->httpStatusCode !== 200) {
			$e = new Radian6ApiException(
				array(
					'error_code' => $this->httpStatusCode,
					'error' => array(
						'message' => $result[0],
						'type' => 'Radian6ApiException',
					),
				)
			);

			throw $e;
		}

		return $result;
	}

	/**
	 * Convert Simple XML Obect to a PHP array via a JSON string.
	 *
	 * @param ( mixed ) $xmlObject
	 * @return ( array )
	 */
	protected function xmlToArray($xmlObject) {
		$jsonStr = json_encode($xmlObject);

		return json_decode($jsonStr, true);
	}

	/**
	 * Makes an HTTP request via cURL.
	 *
	 * @param ( str ) $url The URL the request should be made to
	 * @param ( str ) $requestMethod The request method to be used [ 'get' || 'post' ]
	 * @param ( mixed ) $customHeaders The parameters to use for the request headers
	 * @param ( mixed ) $params The parameters to use for the request body
 	 * @param ( CurlHandler ) $ch Initialized curl handle
	 *
	 * @return ( str ) The response text [ XML ]
	 */
	protected function makeRequest($url, $requestMethod = 'get', $customHeaders = array(), $params = array(), $ch = null) {
		if (!$ch) {
			$ch = curl_init();
		}

		$opts = self::$CURL_OPTS;
		if ($requestMethod === 'get') {
			if(!empty($params)) {
				$url .=  '?' . http_build_query($params, null, '&');
			}
		} else if ($requestMethod === 'post') {
			$opts[CURLOPT_POST] = count($params);
			$opts[CURLOPT_POSTFIELDS] = http_build_query($params, null, '&');
		}

		$opts[CURLOPT_URL] = $url;

		// disable the 'Expect: 100-continue' behaviour. This causes CURL to wait
		// for 2 seconds if the server does not support this header.
		if (isset($opts[CURLOPT_HTTPHEADER])) {
			$existing_headers = $opts[CURLOPT_HTTPHEADER];
			$existing_headers[] = 'Expect:';

			/**
			 * Set the custom API request headers.
			 */
			if (!empty($customHeaders)) {
				foreach ($customHeaders as $customHeaderKey => $customHeader) {
					$existing_headers[] = $customHeaderKey . ': ' . $customHeader;
				}
			}

			$opts[CURLOPT_HTTPHEADER] = $existing_headers;
		} else {
			$existing_headers = array('Expect:');

			/**
			 * Set the custom API request headers.
			 */
			if (!empty($customHeaders)) {
				foreach ($customHeaders as $customHeaderKey => $customHeader) {
					$existing_headers[] = $customHeaderKey . ': ' . $customHeader;
				}
			}

			$opts[CURLOPT_HTTPHEADER] = $existing_headers;
		}

		curl_setopt_array($ch, $opts);
		$result = curl_exec($ch);

		// Define the HTTP Status Code and other useful info for the current cURL request.
		if(!curl_errno($ch)) {
			$info = curl_getinfo($ch);

			$this->httpStatusCode = $info['http_code'];
			$this->curlRequest = $info['url'];
		}

		if (curl_errno($ch) == 60) { // CURLE_SSL_CACERT
			$e = new Radian6ApiException(
				array(
					'error_code' => curl_errno($ch),
					'error' => array(
						'message' => curl_error($ch),
						'type' => 'CurlException',
					),
				)
			);
			curl_close($ch);
			throw $e;
		}

		if ($result === false) {
			$e = new Radian6ApiException(
				array(
					'error_code' => curl_errno($ch),
					'error' => array(
						'message' => curl_error($ch),
						'type' => 'CurlException',
					),
				)
			);
			curl_close($ch);
			throw $e;
		}

		curl_close($ch);

		return $result;
	}

}

?>